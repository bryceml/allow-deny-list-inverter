#!/usr/bin/env python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import sys, ipaddress


def exit_with_error():
    print(
        "Something went wrong, this program expects either 1 network per line on standard input, or in a text file provided as the first argument.",
        file=sys.stderr,
    )
    exit(1)


initial_list = []

if len(sys.argv) == 1:
    for line in sys.stdin:
        initial_list.append(ipaddress.ip_network(line.rstrip()))
elif len(sys.argv) == 2:
    f = open(sys.argv[1], "r")
    for line in f.readlines():
        initial_list.append(ipaddress.ip_network(line.rstrip()))
    f.close()
else:
    exit_with_error()

current_network_list = []
if isinstance(initial_list[0], ipaddress.IPv6Network):
    current_network_list.append(ipaddress.ip_network("::/0"))
elif isinstance(initial_list[0], ipaddress.IPv4Network):
    current_network_list.append(ipaddress.ip_network("0.0.0.0/0"))
else:
    exit_with_error()

for network in initial_list:
    loop_list = []
    for network2 in current_network_list:
        if network2.overlaps(network):
            if network2.subnet_of(network):
                continue
            else:
                loop_list.extend(list(network2.address_exclude(network)))
        else:
            loop_list.append(network2)
    current_network_list = loop_list

current_network_list.sort()

for network in current_network_list:
    print(network)
