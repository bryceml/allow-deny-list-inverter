# Installation

```bash
pip install git+https://gitlab.com/bryceml/allow-deny-list-inverter.git
```

# Usage

This lets you create a deny list based off of an allow list, or vice versa.  For example, if you had a deny list like:

```txt
10.0.0.0/8
192.168.0.0/16
```

you could get a corresponding allow-list by doing

```bash
echo "10.0.0.0/8
192.168.0.0/16"|allow-deny-list-inverter
```

which would output:

```txt
0.0.0.0/5
8.0.0.0/7
11.0.0.0/8
12.0.0.0/6
16.0.0.0/4
32.0.0.0/3
64.0.0.0/2
128.0.0.0/2
192.0.0.0/9
192.128.0.0/11
192.160.0.0/13
192.169.0.0/16
192.170.0.0/15
192.172.0.0/14
192.176.0.0/12
192.192.0.0/10
193.0.0.0/8
194.0.0.0/7
196.0.0.0/6
200.0.0.0/5
208.0.0.0/4
224.0.0.0/3
```

It also works with ipv6:

```bash
echo "2000::/3"|allow-deny-list-inverter
```

```txt
::/3
4000::/2
8000::/1
```

You could also have that in a text file and do:

```bash
allow-deny-list-inverter ./example.txt
```
