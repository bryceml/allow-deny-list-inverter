from setuptools import setup, find_packages
import sys, os

version = "0.1"

setup(
    name="allow_deny_list_inverter",
    version=version,
    description="Returns an allow-list for a given deny-list, or vice versa.",
    long_description="""\
This is intended to be used with aws security groups where you have a deny-list, but security groups only support allow-lists.""",
    classifiers=[],  # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    keywords="ip ipaddress ipnetwork denylist allowlist aws security-group",
    author="Bryce Larson",
    author_email="bryceml@bryceml.us",
    url="https://gitlab.com/bryceml/allow-deny-list-inverter",
    license="MPL",
    packages=find_packages(exclude=["ez_setup", "examples", "tests"]),
    include_package_data=True,
    zip_safe=True,
    scripts=["scripts/allow-deny-list-inverter"],
    install_requires=[
        # -*- Extra requirements: -*-
    ],
    entry_points="""
      # -*- Entry points: -*-
      """,
)
